const dotenv = require('dotenv');
dotenv.config();
const assert = require('assert');
const AuthController = require('controller/auth');

describe('Auth Controller Test', () => {
  const authCtrl = new AuthController();

  it('token test', async () => {
    const token = authCtrl.createToken('123');

    AuthController.verifyToken('123');

    const [isValid, userId] = AuthController.verifyToken(token);
    assert.equal(isValid, true);
  });

  it('signIn success test', async () => {
    const [success, token, err] = await authCtrl.signIn('abcd', '1234');
    assert.equal(success, true);
  });

  it('signUp overlap test', async () => {
    const obj = {
      userId: 'abcd',
      password: '1234',
      name: 'test'
    };
    const [success, error] = await authCtrl.signUp(obj);
    assert.equal(success, false);
  });
});