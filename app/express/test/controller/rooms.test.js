const dotenv = require('dotenv');
dotenv.config();
const assert = require('assert');
const RoomsController = require('controller/rooms');

describe('Rooms Controller Test', () => {
  const roomsCtrl = new RoomsController();
  it('get rooms', async () => {
    const ret = await roomsCtrl.getRooms();
    console.log(ret);
  });
});
