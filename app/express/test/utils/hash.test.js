const assert = require('assert');
console.log(require('utils/hash'));
const { md5, hashPassword, randomHash, randomSalt } = require('utils/hash');

describe('hash test', () => {
  it('md5 test', () => {
    assert.equal(md5('123'), '202cb962ac59075b964b07152d234b70');
  });

  it('hashPassword test', () => {
    const hash = '83ba0fca1bc38fb6bc061942b0a3f94ef5a767e02c66dce5ccbbf304341378839248ca32dbaaaaf427eea33dc8fb13355346f161ac0e22886ffd7e71115f29b4';    
    assert.equal(hashPassword('1234', '12345'), hash);
  });

  it('randomHash test', () => {
    const random = randomHash();
    assert.equal(random.length, 40);
    assert.notEqual(randomHash(), randomHash());
  });

  it('randomSalt test', () => {
    assert.notEqual(randomSalt(), randomSalt());
  });
});