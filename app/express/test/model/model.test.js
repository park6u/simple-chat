const { Users } = require('model');
const { md5, hashPassword, randomSalt } = require('utils/hash'); 

describe('model crud test', () => {
  it('Users findAll test', async () => {
    const ret = await Users.findAll();
    console.log(ret);
  });
  it('Users Insert test', async () => {
    const salt = randomSalt();
    const userObject = {
      userId: md5('abcd'),
      password: hashPassword('1234', salt),
      salt: salt,
      userName: 'test-name',
    };
    const insert = await Users.create(userObject);
    console.log(insert);

    const ret = await Users.findAll();
    console.log(ret);
  });
});