const sequelize = require('model/seqelize');

describe('Database Connection Test', () => {
  it('first connection test', async () => {
    try {
      await sequelize.authenticate();
      console.log('Connection has been established successfully.');
    } catch (error) {
      console.error('Unable to connect to the database:', error);
    }
  })
});