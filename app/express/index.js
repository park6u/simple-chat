// set environment variable
const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const { Server } = require('socket.io');
const MODE = process.env.MODE;
const http = require('http');
const logger = require('morgan');
const router = require('./src/routes');
const socketRouter = require('./src/routes/chat');

const app = express();
const httpServer = http.createServer(app);
const PORT = '4401';

// setting express midleware
app.set('port', PORT);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// create router listener for server
router.forEach(({ route, path }) => {
  app.use('/api' +path, route);
});

// create socket.io server
const io = new Server(httpServer, { 
  path: '/chat/'
});

// error handler for socket.io
io.engine.on('connection_error', (err) => {
  console.log(err.req);      // the request object
  console.log(err.code);     // the error code, for example 1
  console.log(err.message);  // the error message, for example "Session ID unknown"
  console.log(err.context);  // some additional error context
});

io.on("connection", (socket) => {
  socketRouter(socket, io);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler for router
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

// create http server
httpServer.listen(app.get('port'), function (req, res) {
  console.log('Server is Running on the port ' + app.get('port'));
});

