const crypto = require('crypto');

// value to md5 hash
const md5 = (v) => crypto.createHash('md5').update(v).digest('hex');

// value to sha512 hash, use for password
const hashPassword = (v, salt) => crypto.pbkdf2Sync(v, salt, 256, 64, 'sha512').toString('hex');

// generate random hash
const randomHash = () => crypto.randomBytes(20).toString('hex');

// generate random sha384 hash, use for salt
const randomSalt = () => crypto.createHash('sha384').update(Math.random().toString()).digest('hex');

module.exports = {
  md5,
  hashPassword,
  randomHash, 
  randomSalt
};