const { QueryTypes } = require('sequelize');
const { sequelize, RoomsChattings } = require('model');
const { randomHash } = require('utils/hash');

class ChatController {
  async sendMessage(userId, roomId, msg) {
    const chatObject = {
      chatId: randomHash(),
      userId: userId,
      roomId: roomId,
      content: msg
    };
    // create chat and retrieve users in rooms
    await RoomsChattings.create(chatObject);
    return chatObject;
  }

  async getChattings(roomId) {
    const query = `
      SELECT rooms_chattings.chat_id AS 'chatId', rooms_chattings.room_id AS 'roomId', rooms_chattings.user_id AS 'userId', 
        users.user_name AS 'userName', rooms_chattings.content AS 'content', 
        rooms_chattings.creation_date AS 'creationDate'
        FROM rooms_chattings LEFT JOIN users ON rooms_chattings.user_id = users.user_id
        WHERE rooms_chattings.room_id='${roomId}' ORDER BY rooms_chattings.creation_date;
    `;
    const ret = await sequelize.query(query, { type: QueryTypes.SELECT });
    return ret;
  }
}

module.exports = ChatController;