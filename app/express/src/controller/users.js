const { QueryTypes } = require('sequelize');
const { sequelize, Users, UsersFriends, FollowRequests } = require('model');
const { md5 } = require('utils/hash');
const { Op } = require("sequelize");

class UsersController {
  constructor(userId) {
    this.userId = userId;
  }

  /**
   * get all Users exclude myself(userId)
   */
  async getUsers() {
    const query = 
    `SELECT users.user_id AS 'userId', users.user_name AS 'userName', users.creation_date as 'creationDate', 
      COUNT(users_friends.user_id) AS 'friendsCount', ifnull(users_friends.friend_id, 0) AS 'isFriend',
      ifnull(follow_requests.receiver_id, 0) AS 'requestDone'
      FROM users 
      LEFT JOIN users_friends ON users.user_id =  users_friends.user_id 
      LEFT JOIN follow_requests ON users.user_id = follow_requests.receiver_id
      WHERE users.user_id != '${md5(this.userId)}' GROUP BY users.user_id;`
    const ret = await sequelize.query(query, { type: QueryTypes.SELECT });
    return ret;
  }

  async getUser(userId) {
    const ret = await Users.findOne({
      where: {
        userId
      }
    })
    return ret;
  }

  async getUsersFriends() {
    const query = `
    SELECT users_friends.user_id AS 'userId', users_friends.friend_id AS 'friendId', users.user_name AS 'friendName',
      users.creation_date AS 'creationDate', COUNT(users_friends.user_id) AS 'friendsCount'
      FROM users_friends 
      LEFT JOIN users ON users_friends.friend_id = users.user_id WHERE users_friends.user_id='${this.userId}'
      GROUP BY users.user_id;
    `;
    const ret = await sequelize.query(query, { type: QueryTypes.SELECT });
    return ret
  }

  /**
   * accept following request from users
   * create two records in table
   */
  async acceptFollowRequest(senderId, receiverId) {
    try {
      await FollowRequests.destroy({
        where: {
          'senderId': receiverId,
          'receiverId': senderId
        }
      })

      //create friends
      await Promise.all([
        UsersFriends.findOrCreate({
          where: {
            userId: senderId,
            friendId: receiverId
          }
        }),
        UsersFriends.findOrCreate({
          where: {
            userId: receiverId,
            friendId: senderId
          }
        })
      ]);
    } catch (error) {
      console.error(error);
    }
    return true;
  }

  async dismissFollowRequest(senderId, receiverId) {
    await FollowRequests.destroy({
      where: {
        'receiverId': senderId,
        'senderId': receiverId
      }
    });
    return true;
  }

  async getFollowerRequests() {
    const query = `
      SELECT follow_requests.sender_id AS 'senderId', follow_requests.receiver_id AS 'receiverId',
      users.user_name AS 'friendName', users.creation_date AS 'creationDate'
      FROM follow_requests
      LEFT JOIN users ON follow_requests.sender_id = users.user_id WHERE follow_requests.receiver_id='${this.userId}' AND follow_requests.acceptance_state=0
    `;
    const ret = await sequelize.query(query, { type: QueryTypes.SELECT });

    return ret;
  }

  async getFollowingRequests() {
    const ret = FollowRequests.findAll({
      where: {
        senderId: this.userId,
        acceptanceState: true
      }
    });

    return ret;
  }

  async addFollowRequest(senderId, receiverId) {
    const followReqObject = {
      senderId,
      receiverId,
      acceptanceState: 0
    };
    const ret = await FollowRequests.create(followReqObject);

    return ret;
  }

  async unfollow(userId, friendId) {
    try {
      await Promise.all([
        UsersFriends.destroy({
          where: {
            'userId': userId,
            'friendId': friendId
          }
        }),
        UsersFriends.destroy({
          where: {
            'friendId': userId,
            'userId': friendId
          }
        })
      ])
    } catch (error) {
      console.error(error);
    }
  }
}

module.exports = UsersController;