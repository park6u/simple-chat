const { QueryTypes, Op } = require('sequelize');
const { sequelize, Rooms, RoomUsers, Users } = require('model');
const { randomHash } = require('utils/hash');

class RoomsController {
  async getRooms() {
    try {
      const query = 
      `SELECT rooms.room_id AS 'roomId', rooms.room_name AS 'roomName', COUNT(room_users.user_id) AS 'usersCount', rooms.is_dm AS 'isDm' FROM rooms 
        LEFT JOIN room_users ON rooms.room_id = room_users.room_id GROUP BY rooms.room_id HAVING rooms.is_dm is null;;`
      const roomsRet = await sequelize.query(query, { type: QueryTypes.SELECT });
      return roomsRet;
    } catch (error) {
      console.error(error);
    }
  }

  async getRoomsUsers(roomId) {
    const query = 
    `SELECT room_users.room_id AS 'roomId', users.user_id AS 'userId', users.user_name AS 'userName' 
      FROM room_users LEFT JOIN users ON room_users.user_id = users.user_id WHERE room_users.room_id = '${roomId}';`
    const ret = await sequelize.query(query, { type: QueryTypes.SELECT });
    return ret;
  }

  async enterRooms(roomId, userId) {
    try {
      await RoomUsers.findOrCreate({
        where: {
          roomId: roomId,
          userId: userId
        }
      });

      return true;
    } catch (error) {
      console.error(error);
    }
  }

  async leaveRooms(roomId, userId) {
    try {
      await RoomUsers.destroy({
        where: {
          roomId,
          userId
        }
      });

      return true;
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * create DM Room with Friends
   * if alreay exist, return created one
   * else, create new dm room
   */
  async createDMRooms(userId, targetId) {
    try {
      const room = await Rooms.findOne({
        where: {
          [Op.or]: [{creatorId: userId}, {creatorId: targetId}]
        }
      });
      if (room !== null) {
        return room.roomId;
      }

      const user = await Users.findOne({
        where: {
          'userId': userId
        }
      });
      const target = await Users.findOne({
        where: {
          'userId': targetId
        }        
      });

      const roomId = randomHash();
      const roomsObject = {
        roomId: roomId,
        creatorId: userId,
        roomName: `${user.userName}님과 ${target.userName}님의 DM 룸`,
        isDm: true
      };

      const ret = await Rooms.create(roomsObject);

      return roomId;
    } catch (error) {
      console.error(error);
    }
  }
}

module.exports = RoomsController;