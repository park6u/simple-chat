const jwt = require('jsonwebtoken');
const SECRET = process.env.SECRET;
const { md5, randomHash, hashPassword, randomSalt } = require('utils/hash');
const { Users, Sessions } = require('model');

class AuthController {
  /**
   * execute sign in
   * create new session
   * @param userId
   * @param password
   */
  async signIn(userId, password) {
    try {
      const ret = await Users.findOne({
        where: {
          userId: md5(userId),
        }
      });

      if (ret === null) {
        // invalid auth info
        // TODO:: recfactor hardcoded error message
        return [false, null, 'invalid auth info'];
      }

      if (ret.password !== hashPassword(password, ret.salt)) {
        return [false, null, 'invalid auth info'];
      }

      const token = this.createToken(userId);
      const secretToken = token.split('.')[2];

      const sessionObject = {
        sessionId: secretToken,
        userId: md5(userId),
        token: token
      };

      await Sessions.create(sessionObject);

      return [true, secretToken, null];
    } catch (error) {
      console.error(error);
      return [false, null, error];
    }
  }

  /**
   * execute sign up
   * insert new user
   * @param {*} param0 
   */
  async signUp({ userId, password, name }) {
    try {
      // userId overlap check
      const ret = await Users.findOne({
        where: {
          userId: md5(userId)
        }
      });
      if (ret !== null)
        return [false, 'already exist'];

      const salt = randomSalt();
      const userObject = {
        userId: md5(userId),
        password: hashPassword(password, salt),
        salt: salt,
        userName: name,
      };

      await Users.create(userObject);

      return [true, null];
    } catch (error) {
      console.error(error);
      return [false, error];
    }
  }

  createToken(userId) {
    return jwt.sign({userId: userId, creationDate: new Date().getTime() }, SECRET, {});
  }

  static async authenticate(req, res, next) {
    const secretToken = req.get('Authorization');

    const { token } = await Sessions.findOne({ where: { sessionId: secretToken } });

    if (!token) {
      res.json({
        message: 'token not exist'
      })
      next();
    }
    const [isValid, userId] = this.verifyToken(token);

    if (!isValid) {
      res.json({
        message: 'invalid token'
      })
      next();
    }

    return [true, userId];
  }

  /**
   * check validation of auth token
   */
  static verifyToken(token) {
    try {
      const dec = jwt.verify(token, SECRET);
      if (new Date().getTime() - dec.creationDate > 10800000)
          return [false, null];
      return [true, dec.userId];
    } catch (error) {
      console.error(error);
      return [false, null];
    }
  }
}

module.exports = AuthController;