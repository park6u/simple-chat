const { DataTypes } = require('sequelize');

const RoomsUsers = {
  roomId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false
  },
  userId: {
    type: DataTypes.STRING,
  },
  creationDate: {
    type: 'TIMESTAMP'
  }
};

module.exports = RoomsUsers;