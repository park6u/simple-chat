const { DataTypes } = require('sequelize');

const Users = {
  userId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false
  },
  userName: {
    type: DataTypes.STRING
  },
  password: {
    type: DataTypes.STRING
  },
  salt: {
    type: DataTypes.STRING
  },
  creationDate: {
    type: 'TIMESTAMP'
  }
};

module.exports = Users;