const { DataTypes } = require('sequelize');

const Sessions = {
  sessionId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false
  },
  userId: {
    type: DataTypes.STRING
  },
  token: {
    type: DataTypes.STRING
  },
  expiresDate: {
    type: 'TIMESTAMP'
  }
};

module.exports = Sessions;