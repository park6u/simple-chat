const { DataTypes } = require('sequelize');

const UsersFriends = {
  userId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false
  },
  friendId: {
    type: DataTypes.STRING,
    allowNull: false
  },
  creationDate: {
    type: 'TIMESTAMP'
  }
};

module.exports = UsersFriends;