const { DataTypes } = require('sequelize');

const RoomsChattings = {
  chatId: {
    type: DataTypes.STRING,
    primaryKey: true,
  },
  roomId: {
    type: DataTypes.STRING,
  },
  userId: {
    type: DataTypes.STRING,
    allowNull: false
  },
  content: {
    type: DataTypes.STRING
  },
  creationDate: {
    type: 'TIMESTAMP'
  }
};

module.exports = RoomsChattings;