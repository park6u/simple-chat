const { Sequelize } = require('sequelize');
const dotenv = require('dotenv');
dotenv.config();

const sequelize = new Sequelize('simple_chat', process.env.DB_USER, process.env.DB_PASSWORD, {
  host:  process.env.MODE === 'development' ? 'localhost' : '',
  dialect: 'mysql'
});

module.exports = sequelize;