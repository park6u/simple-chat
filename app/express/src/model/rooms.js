const { DataTypes } = require('sequelize');

const Rooms = {
  roomId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false
  },
  creatorId: {
    type: DataTypes.STRING
  },
  roomName: {
    type: DataTypes.STRING
  },
  isDm: {
    type: DataTypes.BOOLEAN
  },
  creationDate: {
    type: 'TIMESTAMP'
  }
};

module.exports = Rooms;