const sequelize = require('./seqelize');
const Rooms = require('./rooms');
const Users = require('./users');
const Sessions = require('./sessions');
const RoomUsers = require('./room_users');
const UsersFriends = require('./users_friends');
const FollowRequests = require('./follow_requests');
const RoomsChattings = require('./rooms_chattings');

// models 
const models = [
  { name: 'Rooms', model: Rooms }, 
  { name: 'Users', model: Users },
  { name: 'Sessions', model: Sessions },
  { name: 'RoomUsers', model: RoomUsers },
  { name: 'UsersFriends', model: UsersFriends },
  { name: 'FollowRequests', model: FollowRequests },
  { name: 'RoomsChattings', model: RoomsChattings },
];

// convert models to key-pair object

const modelsObject = Object.fromEntries(models.map(({name, model}) =>
  [name, sequelize.define(name, model, {
    underscored: true,
    timestamps: true,
    updatedAt: false,
    createdAt: 'creationDate'
  })]
));

modelsObject.sequelize = sequelize;

module.exports = modelsObject;