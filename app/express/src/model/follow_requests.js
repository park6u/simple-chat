const { DataTypes } = require('sequelize');

const FollowRequests = {
  senderId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false
  },
  receiverId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false
  },
  creationDate: {
    type: 'TIMESTAMP'
  },
  acceptanceState: {
    type: DataTypes.BOOLEAN
  }
};

module.exports = FollowRequests;