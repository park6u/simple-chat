const express = require('express');
const router = express.Router();
const { md5 } = require('utils/hash');
const AuthController = require('controller/auth');
const RoomsController = require('controller/rooms');
const ChatController = require('controller/chat');

router.get('/', async function(req, res, next) {
  await AuthController.authenticate(req, res, next);

  const roomsCtrl = new RoomsController();

  const ret = await roomsCtrl.getRooms();

  res.json(ret);
});

router.get('/:id/users', async function(req, res, next) {
  await AuthController.authenticate(req, res, next);

  const roomsCtrl = new RoomsController();

  const ret = await roomsCtrl.getRoomsUsers(req.params.id);

  res.json(ret);
}); 

router.post('/friends/:id', async function(req, res, next) {
  const [_, userId] = await AuthController.authenticate(req, res, next);

  if (typeof req.params.id === "undefined")
    res.sendStatus(400);

  const roomsCtrl = new RoomsController();

  const roomId = await roomsCtrl.createDMRooms(md5(userId), req.params.id);

  res.json({
    roomId: roomId
  });
});

router.get('/:id/chat', async function(req, res, next) {
  const [_, userId] = await AuthController.authenticate(req, res, next);
  const chatCtrl = new ChatController();

  const ret = await chatCtrl.getChattings(req.params.id);

  res.json(ret);
});

module.exports = router;