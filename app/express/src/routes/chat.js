const ChatController = require('controller/chat');
const UsersController = require('controller/users');
const RoomsController = require('controller/rooms');

function router(socket, io) {
  const userCtrl = new UsersController();
  const chatCtrl = new ChatController();
  const roomsCtrl = new RoomsController();

  socket.on('join', async (joinObject) => {
    const { roomId, userId } = joinObject;
    console.log(`${userId} join ${roomId}`);
    await roomsCtrl.enterRooms(roomId, userId);
    socket.join(roomId);
    const ret = await roomsCtrl.getRoomsUsers(roomId);
    // broadcast event to all subscriber
    io.in(roomId).emit('users-update', ret);
  });

  socket.on('leave', async (leaveObject) => {
    const { roomId, userId } = leaveObject;
    console.log(`${userId} leave ${roomId}`);
    socket.leave(roomId);
    await roomsCtrl.leaveRooms(roomId, userId);
    const ret = await roomsCtrl.getRoomsUsers(roomId);
    // broadcast event to all subscriber
    io.in(roomId).emit('users-update', ret);
  });

  // receive chat event from client
  socket.on('chat', async (data) => {
    const { roomId, msg, userId } = JSON.parse(data);
    const user = await userCtrl.getUser(userId);
    console.log(`${user.userName} send ${msg}`);

    // store message
    const chatObject = await chatCtrl.sendMessage(userId, roomId, msg);
    chatObject.userName = user.userName;

    // broadcast event to all subscriber
    socket.to(roomId).emit('chat', chatObject);
  });
};

module.exports = router;