const express = require('express');
const router = express.Router();
const AuthController = require('controller/auth');
const UsersController = require('controller/users');

router.get('/', async function(req, res, next) {
  try {
     const [_, userId] = await AuthController.authenticate(req, res, next);

    const userCtrl = new UsersController(userId);
    const ret = await userCtrl.getUsers();

    res.json(ret);
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
});

module.exports = router;