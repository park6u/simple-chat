const authRouter = require('./auth');
const userRouter = require('./users');
const roomsRouter = require('./rooms');
const friendsRouter = require('./friends');

module.exports = [
  {route: authRouter, path: '/auth'},
  {route: userRouter, path: '/users'},
  {route: friendsRouter, path: '/friends'},
  {route: roomsRouter, path: '/rooms'},
];