const express = require('express');
const router = express.Router();
const AuthController = require('../controller/auth');
const { md5 } = require('utils/hash');

router.post('/signin', async function(req, res, next) {
  if (typeof req.body === "undefined") {
    res.sendStatus(500);
  }

  const authCtrl = new AuthController();
  const {userId, password} = req.body;
  const [success, token, err] = await authCtrl.signIn(userId, password);

  if (success) {
    res.json({
      token: token,
      userId: md5(userId)
    });
  } else {
    res.sendStatus(400);
  }
});

router.post('/signup', async function(req, res, next) {
  if (typeof req.body === "undefined") {
    res.sendStatus(500);
  }
  
  const authCtrl = new AuthController();
  const [success, err] = await authCtrl.signUp(req.body);

  if (success) {
    res.sendStatus(200);
  } else {
    res.json({
      message: err
    });
  }

});

module.exports = router;