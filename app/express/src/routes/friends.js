const express = require('express');
const router = express.Router();
const { md5 } = require('utils/hash');
const AuthController = require('controller/auth');
const UsersController = require('controller/users');

router.get('/', async function(req, res, next) {
  const [_, userId] = await AuthController.authenticate(req, res, next);
  const userCtrl = new UsersController(md5(userId));

  const ret = await userCtrl.getUsersFriends();

  res.json(ret);
});

router.get('/requests', async function(req, res, next) {
  const [_, userId] = await AuthController.authenticate(req, res, next);
  const userCtrl = new UsersController(md5(userId));

  const ret = await userCtrl.getFollowerRequests(md5(userId));

  res.json(ret);
});

router.put('/:id', async function(req, res, next) {
  const [_, userId] = await AuthController.authenticate(req, res, next);

  const userCtrl = new UsersController();
  
  if (req.query.accept === '1')
    await userCtrl.acceptFollowRequest(md5(userId), req.params.id);
  else
    await userCtrl.dismissFollowRequest(md5(userId), req.params.id);
  
  res.sendStatus(200);
});


router.post('/:id', async function(req, res, next) {
  const [_, userId] = await AuthController.authenticate(req, res, next);

  const userCtrl = new UsersController();
  await userCtrl.addFollowRequest(md5(userId), req.params.id);

  res.sendStatus(200);
});

router.delete('/:id', async function(req, res, next) {
  const [_, userId] = await AuthController.authenticate(req, res, next);

  const userCtrl = new UsersController();
  await userCtrl.unfollow(md5(userId), req.params.id);

  res.sendStatus(200);
});

module.exports = router;