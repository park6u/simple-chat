const headersConfig = () => ({ 
  'Content-type': 'application/json',
  'Authorization': window.localStorage.getItem('token') 
});

export const getRooms = async () => {
  const res = await fetch('/api/rooms', {
    headers: headersConfig()
  });
  const ret = await res.json();
  return ret;
}

export const getUsers = async () => {
  const res = await fetch('/api/users', {
    headers: headersConfig()
  });
  const ret = await res.json();
  return ret;
}

export const getRoomsUsers = async (id) => {
  const res = await fetch(`/api/rooms/${id}/users`, {
    headers: headersConfig()
  });
  const ret = await res.json();
  return ret;
}

export const getRoomsChatting = async (id) => {
  const res = await fetch(`/api/rooms/${id}/chat`, {
    headers: headersConfig()
  });
  const ret = await res.json();
  return ret;
}

export const getFriends = async () => {
  const res = await fetch('/api/friends', {
    headers: headersConfig()
  });
  const ret = await res.json();
  return ret;
}

export const getFollowRequest = async () => {
  const res = await fetch('/api/friends/requests', {
    headers: headersConfig()
  });
  const ret = await res.json();
  return ret;
}

export const deleteFriend = async (id) => {
  const res = await fetch(`/api/friends/${id}`, {
    method: 'DELETE',
    headers: headersConfig()
  });
  return res;
}

export const postCreateDM = async (id) => {
  const res = await fetch(`/api/rooms/friends/${id}`, {
    method: 'POST',
    headers: headersConfig()
  });
  const ret = await res.json();
  return ret;
}

export const postFolloRequest = async (id) => {
  const res = await fetch(`/api/friends/${id}`, {
    method: 'POST',
    headers: headersConfig()
  });
  return res;
}

export const putFollowRequest = async (id, state) => {
  const res = await fetch(`/api/friends/${id}?accept=${state ? '1' : '0'}`, {
    method: 'PUT',
    headers: headersConfig()
  });
  return res;
}