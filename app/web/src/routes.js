import React, { useCallback } from 'react';
import { Route, Redirect } from 'react-router-dom';

const SignIn = React.lazy(() => import('./pages/signin'));
const SignUp = React.lazy(() => import('./pages/signup'));

const Chat = React.lazy(() => import('./pages/chat'));
const Friends = React.lazy(() => import('./pages/friends'));
const Users = React.lazy(() => import('./pages/users'));
const Rooms = React.lazy(() => import('./pages/rooms'));
const Header = React.lazy(() => import('./components/Header'));

export const AuthLayout = (props) => {
  const onLogoutClick = useCallback((e) => {
    localStorage.removeItem('token');
    window.location.href = '/signin';
  }, []);

  return (
    <React.Fragment>
      <Header onLogoutClick={onLogoutClick}/>
      {props.children}
    </React.Fragment>
  )
}

export const NonAuthRouter = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
      if (localStorage.getItem('token')) {
        return <Redirect to="/rooms" />
      }
  
      return <Component {...props} />
    }} />
);

export const AuthRouter = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
      if (!localStorage.getItem('token')) {
        return <Redirect to="/signin" />
      }
  
      return (
        <AuthLayout>
            <Component {...props} />
        </AuthLayout>
      );
    }} />
);

const nonAuthPath = [
  {path: '/', component: SignIn},
  {path: '/signin', component: SignIn},
  {path: '/signup', component: SignUp},
];

const authPath = [
  {path: '/users', component: Users},
  {path: '/rooms', component: Rooms},
  {path: '/friends', component: Friends},
  {path: '/rooms/:id/chat', component: Chat},
];

export const NonAuthRoute = () => {
    return (
        <React.Fragment>
            {nonAuthPath.map(({path, component}, index) => <NonAuthRouter key={index} path={path} component={component} exact/>)}
        </React.Fragment>
    );
}

export const AuthRoute = () => {
    return (
        <React.Fragment>
            {authPath.map(({path, component}, index) => <AuthRouter key={index} path={path} component={component} exact />)}
        </React.Fragment>
    );
}