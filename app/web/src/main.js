import React from 'react';
import { AuthRoute, NonAuthRoute } from './routes';

export default function Main(props) {
  return (
    <React.Fragment>
      <NonAuthRoute />
      <AuthRoute />
    </React.Fragment>
  );
}
