import React, { useState, useEffect } from 'react';
import UsersList from '../components/UsersList';
import { getUsers, postFolloRequest } from '../repository/api';

function UsersPage(props) {
  const [users, setUsers] = useState([]);

  const dateFormatter = (d) => {
    const date = new Date(d);
    return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()}`
  }

  const _getUsers = async () => {
    const ret = await getUsers();
    setUsers(ret);
  }

  const onFollowRequest = async (e) => {
    await postFolloRequest(e.target.id);
    window.location.reload();
  }

  useEffect(() => {
    _getUsers();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <div className="mt-4">
            <h4>전체 사용자 목록</h4>
            {users.map(({ userId, userName, creationDate, friendsCount, isFriend, requestDone }) => 
              <UsersList 
                key={userId}
                userId={userId}
                name={userName} 
                count={friendsCount}
                requestDone={requestDone}
                isFriend={isFriend}
                creationDate={dateFormatter(creationDate)}
                onFollowRequest={onFollowRequest} />)}
          </div>
        </div>
      </div>
    </div>
  );
}

export default UsersPage;