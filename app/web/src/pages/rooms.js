import React, { useState, useEffect } from 'react';
import RoomsList from '../components/RoomsList';
import { getRooms, enterToRooms } from '../repository/api';

function RoomsPage(props) {
  const [rooms, setRooms] = useState([]);

  const _getRooms = async () => {
    const ret = await getRooms();
    setRooms(ret);
  }

  useEffect(() => {
    _getRooms();
  }, []);

  const onEnterClick = async (e) => {
    const id = e.target.id;
    window.location.href = `/rooms/${id}/chat`;
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <div className="mt-4">
            <h4>채팅방 목록</h4>
            {rooms.map(({ roomId, roomName, usersCount }) => 
              <RoomsList 
                key={roomId} 
                id={roomId} 
                name={roomName} 
                count={usersCount}
                onEnterClick={onEnterClick}/> )}
          </div>              
        </div>
      </div>
    </div>
  );
}

export default RoomsPage;