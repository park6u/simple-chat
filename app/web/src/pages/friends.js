import React, { useState, useEffect } from 'react';
import FriendsList from '../components/FriendsList';
import FriendsRequestList from '../components/FriendsRequestList';
import { getFriends, getFollowRequest, deleteFriend, postCreateDM, putFollowRequest } from '../repository/api';

function FriendsPage(props) {
  const [friends, setFriends] = useState([]);
  const [friendsRequests, setFriendsReq] = useState([]);

  const dateFormatter = (d) => {
    const date = new Date(d);
    return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()}`
  }

  const _getFriends = async () => {
    const ret = await getFriends();
    setFriends(friends => [...friends, ...ret]);
  }

  const _getFollowRequest = async () => {
    const ret = await getFollowRequest();
    setFriendsReq(friendsRequests => [...friendsRequests, ...ret]);
  }

  const onAcceptClick = async (e) => {
    await putFollowRequest(e.target.id, true);
    window.location.reload();
  }

  const onDismissClick = async (e) => {
    await putFollowRequest(e.target.id, false);
    window.location.reload();
  }

  const onDeleteClick = async (e) => {
    await deleteFriend(e.target.id);
    window.location.reload();
  }

  const onCreateDM = async (e) => {
    const ret = await postCreateDM(e.target.id);
    window.location.href=`/rooms/${ret.roomId}/chat`;
  }

  useEffect(() => {
    _getFriends();
    _getFollowRequest();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <div className="mt-4">
            <h4>친구 요청 목록</h4>
            {friendsRequests.length === 0 && <div>친구요청 목록이 없습니다</div>}
            {friendsRequests.map(({ senderId, friendName, creationDate }, i) => 
              <FriendsRequestList 
                key={i} 
                userId={senderId}
                name={friendName}
                creationDate={dateFormatter(creationDate)}
                onAcceptClick={onAcceptClick}
                onDismissClick={onDismissClick}
                />
            )}
          </div>
          <div className="mt-4">
            <h4>친구 목록</h4>
            {friends.length === 0 && <div>친구 목록이 없습니다</div>}
            {friends.map(({ friendId, friendName, creationDate, friendsCount }, i) => 
              <FriendsList 
                key={i}
                userId={friendId}
                name={friendName}
                creationDate={dateFormatter(creationDate)}
                friendsCount={friendsCount}
                onCreateDM={onCreateDM}
                onDeleteClick={onDeleteClick} />)}
          </div>
        </div>
      </div>
    </div>
  );
}

export default FriendsPage;