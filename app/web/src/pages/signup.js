import React, { useState, useCallback } from 'react';

export default function SignUp(props) {
  const [form, setForm] = useState({ userId: '', password: '', passwordConfirm: '', name: ''});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const onFormChange = useCallback((e) => {
    setForm({
      ...form,
      [e.target.id]: e.target.value
    });
  }, [form]);

  async function onClick() {
    if (form.userId.length === 0 || form.password.length === 0 || form.name.length === 0) {
      setError('Input ID/PW/NAME');
      return;
    }

    if (form.password !== form.passwordConfirm) {
      setError('PW and confirm-PW is different');
      return;
    }

    setLoading(true);
    delete form.passwordConfirm;
    const ret = await fetch('/api/auth/signup' , {
      method: 'POST',
      headers: { "Content-type": "application/json" },
      body: JSON.stringify(form)
    });
    if (ret.status !== 200) {
      setLoading(false);
      setError('Fail to Sign In');
      return;
    }
    window.location.href = '/signin';
    setError(null);
    setLoading(false);
  }

  return (
    <React.Fragment>
      <div className="container">
            <div className="d-flex justify-content-center align-items-center" style={{'height': '100vh'}}>
              <div>
                <label htmlFor="id"/>
                <input 
                  id="userId" 
                  className="form-control" 
                  type="text" 
                  placeholder="ID" 
                  value={form.userId} 
                  onChange={onFormChange} 
                  disabled={loading} />
                <label htmlFor="pw"/>
                <input 
                  id="password" 
                  className="form-control" 
                  type="password" 
                  placeholder="PW" 
                  value={form.password} 
                  onChange={onFormChange} 
                  disabled={loading} />
                <label htmlFor="pw"/>
                <input 
                  id="passwordConfirm" 
                  className="form-control" 
                  type="password" 
                  placeholder="confirm PW" 
                  value={form.passwordConfirm} 
                  onChange={onFormChange} 
                  disabled={loading} />                  
                <label htmlFor="name"/>
                  <input 
                  id="name" 
                  className="form-control" 
                  type="text" 
                  placeholder="name" 
                  value={form.name} 
                  onChange={onFormChange} 
                  disabled={loading} />
                <button className="btn btn-primary mt-4 w-100" onClick={onClick}>{loading ? 'Loading' : 'Register'}</button>
                {error && <p className="text-danger">{error}</p>}
              </div>
            </div>
      </div>
    </React.Fragment>
  );
}
