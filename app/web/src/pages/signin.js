import React, { useState, useCallback } from 'react';

export default function SignIn(props) {
  const [form, setForm] = useState({ userId: '', password: ''});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const onFormChange = useCallback((e) => {
    setForm({
      ...form,
      [e.target.id]: e.target.value
    });
  }, [form]);

  const onFormKeyDown = useCallback((e) => {
    if (e.key === 'Enter') {
      onClick();
    }
  }, [form]);

  const onSignUpClick = () => {
    window.location.href = '/signup';
  }

  async function onClick() {
    if (form.userId.length === 0 || form.password.length === 0) {
      setError('Input ID/PW');
      return;
    }

    setLoading(true);
    const ret = await fetch('/api/auth/signin' , {
      method: 'POST',
      headers: { "Content-type": "application/json" },
      body: JSON.stringify(form)
    });
    if (ret.status !== 200) {
      setLoading(false);
      setError('Fail to Sign In');
      return;
    }
    const json = await ret.json();
    localStorage.setItem('token', json.token);
    localStorage.setItem('userId', json.userId);
    window.location.href = '/rooms';
    setError(null);
    setLoading(false);
  }

  return (
    <React.Fragment>
      <div className="container">
            <div className="d-flex justify-content-center align-items-center" style={{'height': '100vh'}}>
              <div>
                <label htmlFor="id"/>
                <input 
                  id="userId" 
                  className="form-control" 
                  type="text" 
                  placeholder="ID" 
                  value={form.userId} 
                  onChange={onFormChange} 
                  disabled={loading} />
                <label htmlFor="pw"/>
                <input 
                  id="password" 
                  className="form-control" 
                  type="password" 
                  placeholder="PW" 
                  value={form.password} 
                  onChange={onFormChange} 
                  onKeyDown={onFormKeyDown}
                  disabled={loading} />
                <button className="btn btn-primary mt-4 w-100" onClick={onClick}>{loading ? 'Loading' : 'Sign In'}</button>
                {error && <p className="text-danger">{error}</p>}
                <button className="btn btn-primary mt-4 w-100" onClick={onSignUpClick}>Sign Up</button>
              </div>
            </div>
      </div>
    </React.Fragment>
  );
}
