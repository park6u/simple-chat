import React, { useState, useEffect, useCallback, useRef, useLayoutEffect } from 'react';
import ChatList from '../components/ChatList';
import { getRoomsChatting, getRoomsUsers } from '../repository/api';
import { io } from 'socket.io-client'

const socket = io('/', {
  path: '/chat'
})

function ChatPage(props) {
  const roomId = props.match.params.id;
  const userId = localStorage.getItem('userId');
  const chattingCard = useRef(null); 
  const [chats, setChats] = useState([]);
  const [users, setUsers] = useState([]);
  const [msg, setMsg] = useState('');

  const _getRoomsChatting = async (roomId) => {
    const ret = await getRoomsChatting(roomId);
    setChats(chats => [...chats, ...ret]);
  }

  const _getRoomsUsers = async (roomId) => {
    const ret = await getRoomsUsers(roomId);
    setUsers(users => [...users, ...ret]);
  }

  function addMessage(msgObject) {
    setChats(chats => [...chats, msgObject]);
  }

  useLayoutEffect(() => {
    chattingCard.current.scrollTop = chattingCard.current.scrollHeight;
  }, [chats]);

  useEffect(() => {
    chattingCard.current.scrollTop = chattingCard.current.scrollHeight;
    _getRoomsChatting(roomId);
    const joinObject = {
      userId,
      roomId
    };
    socket.emit('join', joinObject);
    // receive chat
    socket.on('chat', (msgObject) => {
      addMessage(msgObject);
    });

    socket.on('users-update', (users) => {
      console.log(users);
      setUsers([...users]);
    });

    return () => {
      // onclose page
      socket.emit('leave', joinObject);
    }
  }, []);

  const onFormKeyDown = useCallback((e) => {
    if (e.key === 'Enter') {
      onClick();
    }
  }, [msg]);

  const onChange = (e) => {
    setMsg(e.target.value);
  }

  // send chat
  const onClick = () => {
    if (msg.length === 0) return;
    addMessage({
      userName: '나',
      content: msg
    });
    setMsg('');
    const msgObject = {
      userId,
      roomId,
      msg
    };
    socket.emit('chat', JSON.stringify(msgObject));
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <div className="mt-4">
            <h4>채팅방</h4>
            <div className="row">
              <div className="col-8">
                <div className="card" style={{height: "580px"}}>
                  <div className="card-header">
                    채팅 목록
                  </div>
                  <div className="card-body" ref={chattingCard} style={{overflow: 'scroll'}}>
                    {chats.map(({ userName, content}, i) => <ChatList key={i} userName={userName} msg={content}/> )}
                  </div>
                </div>
                <div className="d-flex mt-2">
                  <input className="form-control col-auto" value={msg} onChange={onChange} onKeyDown={onFormKeyDown} />
                </div>
              </div>
              <div className="col-4">
                <div className="card" style={{height: "580px"}}>
                  <div className="card-header">
                    참가자 목록
                  </div>
                  <div className="card-body">
                    <ul>
                      {users.map(({ userId, userName }, i) => <li key={i}>{userName}</li>)}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ChatPage;