
export default function FriendsRequestList(props) {
  const { name, userId, creationDate, friendsCount, onAcceptClick, onDismissClick } = props;

  return (
    <div className="card" style={{width: "100%"}}>
      <div className="card-body row align-items-center">
        <div className="col-2">
          <span>
            이름: {name}
          </span>
        </div>
        <div className="col-2">
          <span>
            가입날짜: {creationDate}
          </span>
        </div>

        <div className="col-auto">
          <button id={userId} className="btn btn-primary float-right" onClick={onAcceptClick}>수락</button>
          <button id={userId} className="btn btn-primary float-right" onClick={onDismissClick}>거절</button>
        </div>
      </div>              
    </div>
  );
};