
export default function FriendsList(props) {
  const { name, userId, creationDate, friendsCount, onDeleteClick, onCreateDM } = props;
  return (
    <div className="card mb-2" style={{width: "100%"}}>
      <div className="card-body row align-items-center">
        <div className="col-2">
          <span>
            이름:{name}                
          </span>
        </div>
        <div className="col-2">
          <span>
            가입날짜:{creationDate}
          </span>
        </div>
        <div className="col-2">
          <span>
            친구수: {friendsCount}
          </span>
        </div>
        <div className="col-auto">
          <button id={userId} className="btn btn-primary float-right" onClick={onDeleteClick}>친구삭제</button>
          <button id={userId} className="btn btn-primary float-right" onClick={onCreateDM}>DM</button>
        </div>
      </div>              
    </div>
  );
};