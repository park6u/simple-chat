import React, { useMemo, useState } from 'react';
import { NavLink, Link } from 'react-router-dom';

function Header(props) {
    const { onLogoutClick } = props;
    const [open, toggle] = useState(false);
    const showNavigation = useMemo(() => {
      return open ? "show" : "";
    }, [open]);

    const onClick = () => {
      toggle(!open);
    }

    return (
      <header>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <span className="navbar-brand">SimlpeChat</span>
        <button className="navbar-toggler" onClick={onClick}>
          <span className="navbar-toggler-icon"></span>
        </button>
          <div id="menu" className={"collapse navbar-collapse navbar-light bg-light " + showNavigation}>
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item"><NavLink className="nav-link" to="/rooms" activeClassName="active">Rooms</NavLink></li>
              <li className="nav-item"><NavLink className="nav-link" to="/users" activeClassName="active">Users</NavLink></li>
              <li className="nav-item"><NavLink className="nav-link" to="/friends" activeClassName="active">Friends</NavLink></li>
              <li className="nav-item"><Link className="nav-link" to="/" onClick={onLogoutClick}>Logout</Link></li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }

  export default Header;