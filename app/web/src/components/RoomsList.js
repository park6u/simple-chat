
export default function RoomsList(props) {
  const { id, onEnterClick, name, count } = props;
  return (
    <div className="card mb-2" style={{width: "100%"}}>
      <div className="card-body row align-items-center">
        <div className="col-2">
          <span>
            참여인원수: {count}
          </span>
        </div>
        <div className="col-2">
          <span>
            방이름: {name}
          </span>
        </div>
        <div className="col-auto">
          <button id={id} className="btn btn-primary float-right" onClick={onEnterClick}>입장</button>
        </div>
      </div>              
    </div>
  );
};