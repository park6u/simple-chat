export default function ChatList(props) {
  const { userName, msg} = props;

  return (
    <div>
      <strong>{userName}:</strong> {msg}
    </div>
  );
}