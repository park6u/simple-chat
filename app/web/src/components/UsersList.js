
export default function UsersList(props) {
  const { name, userId, creationDate, onFollowRequest, count, isFriend, requestDone } = props;

  return (
    <div className="card mb-2" style={{width: "100%"}}>
      <div className="card-body row align-items-center">
        <div className="col-2">
          <span>
            이름: {name}
          </span>
        </div>
        <div className="col-2">
          <span>
            가입날짜: {creationDate}
          </span>
        </div>
        <div className="col-2">
          <span>
            친구수: {count}
          </span>
        </div>
        <div className="col-auto">
          {isFriend === '0' && <button id={userId} className="btn btn-primary float-right" onClick={requestDone === '0' ? onFollowRequest : null}>
            {requestDone === '0' ? '친구요청' : '요청보냄'}
           </button>}
        </div>
      </div>              
    </div>
  );
};