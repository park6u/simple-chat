import React, { Suspense, useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Main from './main';

const Loading = () => <div class="spinner-border" role="status"></div>

function App(props) {

  const verifyToken = async () => {
  }

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      verifyToken();
    }
  }, []);

  return (
    <BrowserRouter>
      <Suspense fallback={Loading}>
        <Main />
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
