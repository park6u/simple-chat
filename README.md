# simple-chat

소셜 채팅 앱

environment
- node14
- mysql8
- react18
- sequelize v6
- socket.io v4

# setup

## server
```cd app/express```

로컬 DB(mysql) 실행 
```brew services start mysql```

서버 실행
```npm run start:dev```

## web
```cd app/web```

의존성 설치
```npm install```

로컬 빌드 실행
```npm start```